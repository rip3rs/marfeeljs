define(['GraphRender'], function(GraphRender) {

    'use strict';


    describe('Impressions GraphRender()', function() {

        var revenue = new GraphRender('donut', 'impressions', 'container');

        it('Check if colors are light blue and dark blue', function() {

            revenue.setGraphColor();


            expect(revenue.color).toEqual(['#0DC8E3','#0E4F60']);
        });


        it('Check if Graph method is called', function () {

            spyOn(revenue, 'setGraphType');

            revenue.setGraphType();

            expect(revenue.setGraphType).toHaveBeenCalled();

        });


        it('Check if Graph is a donut chart && is it defined', function () {

            revenue.setGraphType();

            expect(revenue.graphTypeId).toBeDefined();
            expect(revenue.graphTypeId).toEqual('donut');

        });

    });

});