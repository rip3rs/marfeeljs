define(['GraphRender','GraphLogic', 'loadJSON'], function(GraphRender, GraphLogic, loadJSON) {

    'use strict';


    describe('Revenue GraphRender()', function () {

        var revenue = new GraphRender('donut', 'revenue', 'container');

        it('Check if colors for the donut are light green and dark green', function () {

            revenue.setGraphColor();

            expect(revenue.color).toEqual(['#65D219', '#006902']);

        });


        it('Check if Graph method is called', function () {

            spyOn(revenue, 'setGraphType');

            revenue.setGraphType();

            expect(revenue.setGraphType).toHaveBeenCalled();

        });


        it('Check if Graph is a donut chart && is it defined', function () {

            revenue.setGraphType();

            expect(revenue.graphTypeId).toBeDefined();
            expect(revenue.graphTypeId).toEqual('donut');

        });

    });

});