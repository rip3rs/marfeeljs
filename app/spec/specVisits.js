define(['GraphRender'], function(GraphRender) {

    'use strict';


    describe('Visits GraphRender()', function() {

        var revenue = new GraphRender('donut', 'visits', 'container');

        it('Check if visits colors are light orange and dark orange', function() {

            revenue.setGraphColor();

            expect(revenue.color).toEqual(['#F9C104','#D45416']);
        });


        it('Check if Graph method is called', function () {

            spyOn(revenue, 'setGraphType');

            revenue.setGraphType();

            expect(revenue.setGraphType).toHaveBeenCalled();

        });


        it('Check if Graph is a donut chart && is it defined', function () {

            revenue.setGraphType();

            expect(revenue.graphTypeId).toBeDefined();
            expect(revenue.graphTypeId).toEqual('donut');

        });

    });

});