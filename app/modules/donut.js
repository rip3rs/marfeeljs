define(['GraphRender', 'd3'], function (GraphRender, d3) {

    var Donut = function (graphColors, graphData, dataTypeId, graphTotal, euroSign) {

        var me = this;

            me.color = graphColors;
            me.graphData = graphData;
            me.width = 200;
            me.height = 200;
            me.radius = Math.min(me.width, me.height) / 2;
            me.donutWidth = 10;
            me.euroSign = euroSign;

        var color = d3.scaleOrdinal().range(me.color);

        var svg = d3.select('#' + dataTypeId + ' .total')
            .append('svg')
            .attr('width', me.width)
            .attr('height', me.height)
            .append('g')
            .attr('transform', 'translate(' + (me.width / 2) + ',' + (me.height / 2) + ')');

        var arc = d3.arc()
            .innerRadius(me.radius - me.donutWidth)
            .outerRadius(me.radius);

        var pie = d3.pie()
            .value(function (d) { return d.count; })
            .sort(function(d) { return d.count; });

        var path = svg.selectAll('path')
            .data(pie(me.graphData))
            .enter()
            .append('path')
            .attr('d', arc)
            .attr('fill', function (d) {
                return color(d.data.label);
            });

        var legend = svg.selectAll('.legend')
            .data(color.domain())
            .enter()
            .append('g')
            .attr('class', 'legend')
            .attr("text-anchor", "middle")
            .attr('transform', "translate(" + 0 + "," + -me.height/10 + ")");

        legend.append('text')
            .style('fill', '#C4C4C4')
            .text(dataTypeId);


        var totalLegend = svg.selectAll('.total-legend')
            .data(color.domain())
            .enter()
            .append('g')
            .attr('class', 'total-legend')
            .attr("text-anchor", "middle")
            .attr('transform', "translate(" + 0 + "," + me.height/15 + ")");

        totalLegend.append('text').text(function () {

            return graphTotal + me.euroSign;

        });

    };

    return Donut;

});