/**
 *
 */

define(['loadJSON'], function (loadJSON) {

    var GraphLogic = function (dataTypeId) {

        'use strict';

        this.dataTypeId = dataTypeId;
        this.data = {};
        this.deviceNames = null;
        this.renderData = {
            total: 0,
            devices: {}
        };

    };

    GraphLogic.prototype.total = function () {
        // var renderData = this.renderData;
        // var data = this.data;

        this.deviceNames.forEach(function(k) {

            this.renderData.devices[k] = {
                'total' : parseInt(this.data[k].total)
            };

            this.renderData.total += parseInt(this.data[k].total);

        }, this);

        // this.renderData = renderData;

        return this.renderData;
    };

    GraphLogic.prototype.percentage = function () {

        for(var device in this.renderData.devices){

            var deviceObj = this.renderData.devices[device];
            deviceObj.percentage = parseInt( deviceObj.total / this.renderData.total * 100 );

        }

        return this.renderData;
    };

    GraphLogic.prototype.getTheData = function () {
        var setJSON,
            dataTypeId = this.dataTypeId,
            setData = this.data,
            setDeviceNames = this.deviceNames;

        loadJSON(function (response) {

            setJSON = JSON.parse(response);

        });

        setData = setJSON[dataTypeId];
        setDeviceNames = Object.keys(setData);

        this.data = setData;
        this.deviceNames = setDeviceNames;
        this.getData = setJSON;

        return this.getData && this.data && this.deviceNames;
    };

    return GraphLogic;
});