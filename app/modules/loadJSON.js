define(function() {

    var loadJSON = function (callback) {

        'use strict';


        var obj = new XMLHttpRequest();

        obj.overrideMimeType("application/json");
        obj.open('GET', 'app/fakeServer.json', false);
        obj.onreadystatechange = function () {

            if (obj.readyState == 4 && obj.status == "200") {
                // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
                callback(obj.responseText);

            }

        };

        obj.send(null);

    };


    return loadJSON;

});