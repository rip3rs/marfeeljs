/**
 *
 */

define(['GraphRender'], function(GraphRender) {

    'use strict';

    function init() {

        var revenue = new GraphRender('donut', 'revenue', 'container');
        revenue.setGraphData();
        revenue.renderTheData();
        revenue.setGraphColor();
        revenue.setGraphType();

        var impressions = new GraphRender('donut', 'impressions', 'container');
        impressions.setGraphData();
        impressions.renderTheData();
        impressions.setGraphColor();
        impressions.setGraphType();

        var visits = new GraphRender('donut', 'visits', 'container');
        visits.setGraphData();
        visits.renderTheData();
        visits.setGraphColor();
        visits.setGraphType();

    }

    init();

});