define(['GraphLogic', 'Donut'], function (GraphLogic, Donut) {

    var GraphRender = function (graphTypeId, dataTypeId, containerId) {

        'use strict';


        this.graphTypeId = graphTypeId;
        this.dataTypeId = dataTypeId;
        this.$container = document.getElementById(containerId);
        this.getData = null;
        this.color = [];
        this.graphData = [];
        this.euroSign = '';
        this.total = 0;

        if(this.dataTypeId === 'revenue') {
            this.euroSign = '€';
        }

    };

    GraphRender.prototype.setGraphColor = function () {

        switch (this.dataTypeId) {
            case 'revenue':
                this.color.push('#65D219', '#006902');
                break;
            case 'impressions':
                this.color.push('#0DC8E3', '#0E4F60');
                break;

            case 'visits':
                this.color.push('#F9C104', '#D45416');
                break;

            default:
                this.color.push('#000000', '#000000');
        }

        return this.color;

    };

    GraphRender.prototype.setGraphType = function () {

        switch(this.graphTypeId) {
            case 'donut':
                var donut = new Donut(this.color, this.graphData, this.dataTypeId, this.total, this.euroSign);
                break;

            default:
                return 'No graph type chosen';
        }

    };

    GraphRender.prototype.setGraphData = function () {

        this.getData = new GraphLogic(this.dataTypeId);
        this.getData.getTheData();
        this.getData.total();
        this.getData.percentage();

        //data required for the donut chart.
        for (var deviceName in this.getData.renderData.devices) {
            var deviceObj = this.getData.renderData.devices[deviceName];

            this.graphData.push({label: deviceName, count: deviceObj.total});
        }
    };

    GraphRender.prototype.renderTheData = function () {

        var $container = this.$container,
            $mainElement = document.createElement('div'),
            $graphMainElement = document.createElement('div'),
            $deviceContainerElement = document.createElement('div'),
            deviceName;

        $mainElement.id = this.dataTypeId;
        $mainElement.className = 'container ' + this.graphTypeId;
        $container.appendChild($mainElement);

        $graphMainElement.className = 'total';
        $mainElement.appendChild($graphMainElement);

        $deviceContainerElement.className = 'devices';
        $mainElement.appendChild($deviceContainerElement);

        for (deviceName in this.getData.renderData.devices) {

            var deviceObj = this.getData.renderData.devices[deviceName],
                $deviceElement = document.createElement('div'),
                $deviceNameElement = document.createElement('div'),
                $deviceDataContainerElement = document.createElement('div'),
                $percentageDataElement = document.createElement('div'),
                $totalDataElement = document.createElement('div');

            $deviceElement.className = deviceName;
            $deviceContainerElement.appendChild($deviceElement);

            $deviceNameElement.className = 'name';

            //condition to change color based on device
            switch(deviceName) {
                case 'tablet':
                    $deviceNameElement.style.color = this.color[0];
                    break;

                case 'smartphone':
                    $deviceNameElement.style.color = this.color[1];
                    break;

                default:
                    return 'No color';
            }

            $deviceElement.appendChild($deviceNameElement);
            $deviceNameElement.innerHTML += deviceName;

            $deviceDataContainerElement.className = 'data';
            $deviceElement.appendChild($deviceDataContainerElement);

            $percentageDataElement.className = 'percentage';
            $deviceDataContainerElement.appendChild($percentageDataElement);
            $percentageDataElement.innerHTML += deviceObj.percentage + '%';

            $totalDataElement.className = 'total';
            $deviceDataContainerElement.appendChild($totalDataElement);

            var totalDevice = deviceObj.total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
            $totalDataElement.innerHTML += totalDevice + this.euroSign;

        }

        //overall total
        this.total = this.getData.renderData.total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");

    };


    return GraphRender;

});