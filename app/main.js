// Place third party dependencies in the lib folder
//
// Configure loading modules from the lib directory,
// except 'app' ones,
requirejs.config({

    baseUrl: 'app/',

    paths: {
        loadJSON: 'modules/loadJSON',
        GraphRender: 'view/graphRender',
        GraphLogic: 'modules/graphLogic',
        Donut:  'modules/donut',
        d3: 'vendor/d3.min'
    }

});


// Load the main app module to start the app
requirejs([
    'start'
]);