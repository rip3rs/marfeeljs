module.exports = function(grunt) {

    grunt.initConfig({

        less: {

            development: {
                options: {
                    compress: false
                },
                files: { // destination file and source files
                    "app/css/styles.css": "app/less/all.less"
                }
            },

            production: {
                options: {
                    compress: true
                },
                files: { // destination file and source files
                    "app/css/styles.css": "app/less/all.less"
                }
            }

        },

        jasmine: {
            taskName: {
                src: 'app/',
                options: {
                    specs: 'app/spec/**/*.js',
                    template: require('grunt-template-jasmine-requirejs'),
                    templateOptions: {
                        requireConfigFile: 'app/main.js'
                    }
                }
            }
        },

        jshint: {
            all: [
                'Gruntfile.js',
                'app/**/*.js',
                '!app/vendor/*.js'
            ]
        },

        watch: {
            styles: {
                files: ['app/**/*.less'], // which files to watch
                tasks: ['less:development'],
                options: {
                    nospawn: true
                }
            },

            lint: {
                files: ['app/**/*.js'], // which files to watch
                tasks: ['jshint'],
                options: {
                    nospawn: true
                }
            }
        }

    });


    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-jasmine');
    grunt.loadNpmTasks('grunt-contrib-jshint');


    grunt.registerTask('default', ['less:development', 'jshint', 'watch']);
    grunt.registerTask('testAll', ['jasmine', 'jshint']);
    grunt.registerTask('UnitTest', ['jasmine']);
    grunt.registerTask('HintTest', ['jshint']);
};
