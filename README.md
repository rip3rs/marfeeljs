# Marfeel Test

### Prerequisities

`Vagrant`

### Instalation

$`vagrant up`

$`sudo vim /etc/hosts`

paste `192.168.33.15   marfeeljstest.dev`

### Running tests

$`vagrant ssh`

It should go straight to the working folder inside of the box
`/var/www/marfeeljstest.dev`

$`grunt` to compile less

$`grunt testAll` for Jasmine and Jshint

$`grunt UnitTest` for Jasmine only

$`grunt HintTest` for JsHint only

### Run the page

`http://marfeeljstest.dev`