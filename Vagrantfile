# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure(2) do |config|
  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://atlas.hashicorp.com/search.
  config.vm.box = "ubuntu/trusty64"

  config.vm.box_check_update = true

  config.vm.network "private_network", ip: "192.168.33.15"

  config.vm.provider "virtualbox" do |vb|
    # Name the box
    vb.name = "Marfeel Test box"

    # Customize the amount of memory on the VM:
    vb.memory = "4096"
  end

  config.vm.provision "shell", inline: <<-SHELL

    # Variables

    # Enables sites
    PAGE_CONF="marfeeljstest.dev"

    # Shell Script
    echo "##########################################"
    echo "#            APT-GET UPDATE              #"
    echo "##########################################"
    echo " "
    sudo apt-get -y update && sudo apt-get -y upgrade
    echo " "
    echo " "

    echo "##########################################"
    echo "#           GIT & ANT & UNZIP            #"
    echo "##########################################"
    echo " "
    sudo apt-get install -y git ant php5-curl php5-gd unzip
    echo " "
    echo " "

    echo "##########################################"
    echo "#            LAMP INSTALL                #"
    echo "##########################################"
    echo " "
    sudo debconf-set-selections <<< 'lamp-server mysql-server/root_password password root'
    sudo debconf-set-selections <<< 'lamp-server mysql-server/root_password_again password root'
    sudo apt-get install -y lamp-server^
    echo " "
    echo " "

    echo "##########################################"
    echo "#            NODEJS 5.x REPO             #"
    echo "##########################################"
    echo " "
    curl -sL https://deb.nodesource.com/setup_5.x | sudo -E bash -
    echo " "
    echo " "

    echo "##########################################"
    echo "#            NODEJS INSTALL              #"
    echo "##########################################"
    echo " "
    sudo apt-get install -y nodejs
    echo " "
    echo " "

    echo "##########################################"
    echo "#            NPM grunt-cli                   #"
    echo "##########################################"
    echo " "
    npm install -g grunt-cli
    echo " "
    echo " "

    echo "##########################################"
    echo "#            NPM less-cli                #"
    echo "##########################################"
    echo " "
    npm install -g less-cli
    echo " "
    echo " "

    echo "##########################################"
    echo "#        Making the directory            #"
    echo "##########################################"
    echo " "
    sudo mkdir /var/www/$PAGE_CONF
    sudo chown vagrant:vagrant /var/www/$PAGE_CONF
    echo " "
    echo " "

    echo "##########################################"
    echo "#     Move files into the folder         #"
    echo "##########################################"
    echo " "
    rsync -a --progress /vagrant/* /var/www/$PAGE_CONF
    echo " "
    echo " "

    echo "##########################################"
    echo "#         installing node modules        #"
    echo "##########################################"
    echo " "
    cd /var/www/$PAGE_CONF
    npm i
    echo " "
    echo " "

    echo "##########################################"
    echo "#          Setup apache hosts            #"
    echo "##########################################"
    echo " "
    cp /vagrant/config/apache/$PAGE_CONF.conf /etc/apache2/sites-available/
    echo "cp /vagrant/config/apache/$PAGE_CONF.conf /etc/apache2/sites-available/"
    echo "Copied!"
    echo " "
    echo " "

    echo "##########################################"
    echo "#        Setup apache permissions        #"
    echo "##########################################"
    echo " "
    echo "Setting apache user:group"
    sudo bash -c 'echo "User vagrant" >> /etc/apache2/apache2.conf ; echo "Group vagrant" >> /etc/apache2/apache2.conf'
    echo "User vagrant"
    echo "Group vagrant"
    echo "Done!"
    echo " "
    echo " "

    echo "##########################################"
    echo "#            Enable Sites                #"
    echo "##########################################"
    echo " "
    sudo a2ensite $PAGE_CONF
    sudo a2enmod rewrite
    echo " "
    echo " "

    echo "##########################################"
    echo "#            Restart Apache              #"
    echo "##########################################"
    echo " "
    echo "Restarting Apache."
    sudo service apache2 restart
    echo " "
    echo " "

    echo "##########################################"
    echo "#   Setup direct path to working folder  #"
    echo "##########################################"
    echo " "
    echo "cd /var/www/$PAGE_CONF" >> /home/vagrant/.bashrc
    echo "cd /var/www/$PAGE_CONF" >> /home/vagrant/.bashrc
    echo " "
    echo " "

    echo "##########################################"
    echo "#   Vagrant UP is complete!11oneeleven   #"
    echo "##########################################"

  SHELL

end
